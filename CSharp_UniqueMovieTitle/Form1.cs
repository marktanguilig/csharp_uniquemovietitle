﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CSharp_UniqueMovieTitle
{
    public partial class frmmain : Form
    {
        public frmmain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StreamReader content = new StreamReader("titles.txt");
            string line = null;
            line = content.ReadLine();
            while (((line != null)))
            {
                listBox1.Items.Add(line + " - " + line.ToUpper().Replace("THE", "D'").Replace("[", ":").Replace("TWO", "2").Replace("THREE", "3").Replace("[", ":").Replace("FOUR", "4").Replace("FIVE", "5").Replace("SIX", "6").Replace("SEVEN", "7").Replace("EIGHT", "EIGHT").Replace("NINE", "9").Replace(" ", "").Replace("'", "").Replace("i", "").Replace("e", "").Replace("u", "").Replace("E", "").Replace("U", "").Replace("III", "3").Replace("LOVE","LUV"));
                line = content.ReadLine();
            }
        }
    }
}
